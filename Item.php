<?php

require_once "Writable.php";

abstract class Item implements Writable
{
    protected $title;
    protected static $type;
    protected $price;

    public function getTitle()
    {
        return $this->title;
    }

    public static function getType()
    {
        return static::$type = 'base_item';
    }

    public function __construct($title, $price,$type)
    {
        $this->title = $title;
        $this->price = $price;
        static::getType($type);
    }

    abstract public function getPrice();
}