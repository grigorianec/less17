<?php

class ItemsWriter
{
    public $items = [];

    public function addItem(Writable $items)
    {
        $this->items[] = $items;

    }

    public function write()
    {
        $html = '';

        foreach ($this->items as $item) {

            $html .= $item->getSummaryLine() . '<br>';
        }

        return $html;
    }
}