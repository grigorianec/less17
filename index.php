<?php
require_once 'Application.php';
require_once 'Writable.php';
require_once 'Item.php';
require_once 'Food.php';
require_once 'GoodsItem.php';
require_once 'Bonus.php';
require_once 'ItemsWriter.php';
require_once 'Factory.php';

Application::init();

$sql = 'SELECT * from list';
$query = Application::$pdo->prepare($sql);
$query->execute();
$foods = $query->fetchAll();


$writer = new ItemsWriter();
foreach ($foods as $foo) {

    if ($foo['type'] == 'food') {
        $result = new Food($foo['title'], $foo['price'], $foo['type']);
    }elseif ($foo['type'] == 'goods'){
        $result = new GoodsItem($foo['title'],$foo['price'],$foo['type'],$foo['discount']);
    }elseif ($foo['type'] == 'shoes'){
        $result = new Bonus($foo['title'],$foo['type'],$foo['description']);
    }
    $writer->addItem($result);
}

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
          crossorigin="anonymous">
    <link rel="stylesheet" href="main/main.css">
    <title>Главная</title>
</head>
<body>
<?php include_once ('header.php')?>
<section>
    <div class="container main">
        <div class="row">
            <div class="col text-center">
                <?=$writer->write();?>
            </div>
        </div>
    </div>
</section>
<?php include_once ('footer.php')?>
</body>
</html>
