<?php

class GoodsItem extends Item
{
    protected $discount;

    public function __construct($title, $price,$type, $discount)
    {
        parent::__construct($title, $price,$type);
        $this->discount = $discount;
    }

    public static function getType()
    {
        return static::$type = 'goods';
    }

    public function getPrice()
    {
        return $result = $this->price - $this->discount;
    }

    public function getSummaryLine()
    {
        $html = '<p>';

        $html .= 'Товары :' . $this->getTitle() . '<br>';
        $html .= 'Тип товара :' . static::getType() . '<br>';
        $html .= 'Цена :' . $this->getPrice() . '<br>';

        $html .= '</p>';

        return $html . '';

    }
}