<?php
require_once'Application.php';
Application::init();

try {
    $sql = 'CREATE TABLE list (
        id INT NOT NULL auto_increment,
        `title` text,
        `type` text,
		`price` text,
		`discount`int,
		`description`text,
		PRIMARY KEY(id)
	)';
    Application::$pdo->exec($sql);

    $sql = 'INSERT INTO list (`title`,`type`,`price`,`discount`,`description`)VALUES
        ("Креветки","food","100","0","w"),
        ("Пальто","goods","300","10","w"),
        ("Кроссовки","shoes","200","0","Очень удобная обувь")';
    Application::$pdo->exec($sql);

    echo 'Migrate success';
}catch (Exception $e) {
    echo $e->getMessage();
}
