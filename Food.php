<?php

class Food extends Item
{

    public static function getType()
    {
        return static::$type = 'food';
    }

    public function getPrice()
    {
        return $newPrice = $this->price - 10 . '$';
    }

    public function getSummaryLine()
    {
        $html = '<p>';

        $html .='Пища :' . $this->getTitle() . '<br>';
        $html .= 'Тип товара :' . static::getType() . '<br>';
        $html .= 'Цена :' . $this->getPrice() . '<br>';

        $html .= '</p>';

        return $html . '';
    }
}