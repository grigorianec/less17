<?php

class Bonus implements Writable
{
    public $title;
    protected $type;
    protected $description;

    public function __construct($title,$type,$description)
    {
        $this->title = $title;
        $this->type = $type;
        $this->description = $description;
    }

    public function getSummaryLine()
    {
        $html = '<p>';

        $html .= 'Обувь :' . $this->title . '<br>';
        $html .= 'Тип товара :' . $this->type . '<br>';
        $html .=  'Описание :' . $this->description . '<br>';
        $html .= '</p>';

        return $html . '';
    }

}